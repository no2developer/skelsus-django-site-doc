Instalar dependencias nodejs

    npm install

Abrir folder settings

    ├── frontend
    │   ├── site
    │   │   ├── settings
    │   │   │   ├── base.js
    │   │   │   ├── dev.jsa
    │   │   │   ├── prod.js

Modificar valor de los archivos dependiendo del entorno de deployment

    var settings = {
      "project_name": "Skelsus",
      "analytics_ua": "",
      "static_uri" : "/static",
      "root_deploy" : "", 
      "template_ext": ".html"
    }


Iniciar servidor estático

    grunt connect

Iniciar watch de cambios

    grunt watch