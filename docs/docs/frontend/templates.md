** Estructura **

    ├── frontend
    │   ├── site
    │   │   ├── templates
    │   │   │   ├── _layout.jade
    │   │   │   ├── includes
    │   │   │   │   ├── _footer.jade
    │   │   │   │   ├── _header.jade
    │   │   │   ├── sections


#Layout

Es la plantilla principal de la cual **heredan** todos los demás templates. Están
dividas por bloques "block" que pueden ser reescritos en cualquier template.

![Layout](/docs/images/templates/layout.png   "Ejemplo de layout")



** Blocks **

** Styles **

Se utiliza para insertar los estilos. Por defecto tiene 


** Content **

Se utiliza para escribir todo el layout interno.


** Header **

Se utiliza para escribir el contenido de la cabecera. Por defecto hace un include
del archivo includes/_header.jade


** Main **

Se utiliza para escribir el contenido principal.


** Footer **

Se utiliza para escribir el contenido del footer. Por defecto hace un include
del archivo includes/_footer
.jade

** Addons **

Se utiliza para agregar scripts externos. Por ejemplo Facebook, Twitter o Google +.
Por defecto contiene el script externo de Facebook

** Scripts **

Se utiliza para agregar scripts internos.

#Header

Se utiliza para definir el código por defecto para el header.

![Header](/docs/images/templates/header.PNG   "Ejemplo de Header")

#Footer

Se utiliza para definir el código por defecto para el footer.

![Footer](/docs/images/templates/footer.PNG   "Ejemplo de Footer")