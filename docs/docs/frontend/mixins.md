
Un mixin es un utilitario o helper que nos en términos de programación nos ayuda a reutilizar código de manera fácil y estandarizada. En nuestro flujo frontend lo aplicamos para formularios, para embeber etiquetas o para embeber código. Cabe mencionar que los mixins también pueden recibir parámetros.

## Mixins web

###  Mixin social

Éste mixin es utilizado para poder cargar las librerias JS de las redes sociales básicas: Facebook, Twitter y Google+. 

Éste mixin recibe 1 parámetro del tipo Array, en el cual debemos indicar las redes sociales que deseamos incrustar, a continuación los ejemplos:

Si deseamos incrustar solo Facebook, el código sería:

    +social(['fb'])

 Y si desearamos agregar Facebook y Twitter sería:

    +social(['fb', 'tw'])

###  Mixin tracking

Es utilizado para embeber el código UA de Google Analytics. No recibe ningún argumento, para su correcto funcionamiento es necesario que se haya configurado previamente el atributo **analytics_ua** dentro del archivo **base.js**, el cual se encuentra en:  

    ├── frontend
    │   ├── site
    │   │   ├── settings
    │   │   │   ├── base.js

Para usar éste mixin solo se coloca

    +tracking

Y por lo general esta ubicado en **_layout.jade**, en la ruta

    ├── frontend
    │   ├── site
    │   │   ├── templates
    │   │   │   ├── _layout.jade

###  Mixin requireTag

Éste mixin requiere de un parámetro que es el nombre de nuestro archivo JS que deseamos incluir en nuestro Jade.

Es importante mencionar que en éste mixin además de incluir nuestro modulo de Require, se incluye todo el archivo require-config como si fuera Javascript inline.

Su forma de uso es la siguiente:

    +requireTag('site.home')

## Mixins para Formularios

Para el uso de mixins en formularios, existen mixins especiales para cada tipo de input. La lista es la siguiente:

- input_text(config)
- input_date(config)  
- input_number(config)  
- input_email(config)  
- input_search(config)  
- input_file(config)
- list_checkbox(config)  
- checkbox(config)  
- list_radiobox(config)  
- radiobox(config)  
- select(config)  


Cada uno recibe por parámetro un objeto “**config**” con las propiedades de cada mixin. Las propiedades pueden variar entre uno y otro, sin embargo existen algunas propiedades en común, a continuación las propiedades detalladas:


### input_text  
  Éste mixin se utiliza para colocar un input del tipo text. Sus atributos son los siguientes:

  - name
  > valor por defecto = ""  
    Ésta propiedad es el nombre del input, debe ser el mismo que se ha declarado de lado de **backend**.

  - id
  > valor por defecto = name  
    Ésta propiedad asigna el atributo **id** del input, en caso no esté asignado tomará por defecto el valor de  name.
  
  - minLength
  > valor por defecto = 3
    Ésta propiedad asigna la cantidad mínima de caracteres con las que debe contar el input.
  
  - defaultValue
  > valor por defecto = ""  
    Ésta propiedad indica el valor por defecto que tendrá el input.
  
  - isDisabled
  > valor por defecto = false  
    Ésta propiedad indica si el input está deshabilitado o no.
  
  - hasNoLabel
  > valor por defecto = false  
    Ésta propiedad indica si el input no tiene **label**
  
  - placeholder
  > valor por defecto = ""  
    Ésta propiedad asigna un placeholder al input.
  
  - label
  > valor por defecto = ""  
    Ésta propiedad indica el valor del label para el input. 
   
### input_date  
  Éste mixin generea un input del tipo date, el cual solo permite el ingreso de fechas. Sus atributos son los siguientes.

  - name
  > valor por defecto = ""  
    Ésta propiedad es el nombre del input, debe ser el mismo que se ha declarado de lado de **backend**.

  - id
  > valor por defecto = name  
    Ésta propiedad asigna el atributo **id** del input, en caso no esté asignado tomará por defecto el valor de  name.
  
  - minLength
  > valor por defecto = 3
    Ésta propiedad asigna la cantidad mínima de caracteres con las que debe contar el input.
  
  - defaultValue
  > valor por defecto = ""  
    Ésta propiedad indica el valor por defecto que tendrá el input.
  
  - isDisabled
  > valor por defecto = false  
    Ésta propiedad indica si el input está deshabilitado o no.
  
  - hasNoLabel
  > valor por defecto = false  
    Ésta propiedad indica si el input no tiene **label**
  
  - placeholder
  > valor por defecto = ""  
    Ésta propiedad asigna un placeholder al input.
  
  - label
  > valor por defecto = ""  
    Ésta propiedad indica el valor del label para el input. 

  - dateFormat
  > valor por defecto = 'dd/mm/yyyy'  
    Ésta propiedad especifica el formato de fecha que se utilizará, por defecto es día, mes y año.
  
  - minDate
  > valor por defecto = '01/01/1920'  
    Especifica una fecha mínima aceptada, éste valor debe respetar el formato de fecha especificado anteriormente.
  
  - maxDate
  > valor por defecto = false
    Especifica una fecha máxima aceptada. Por defecto no existe éste límite.

### input_number  

  Éste mixin genera un input que solo permite el ingreso de números. Sus atributos son los siguientes.

  - name
  > valor por defecto = ""  
    Ésta propiedad es el nombre del input, debe ser el mismo que se ha declarado de lado de **backend**.

  - id
  > valor por defecto = name  
    Ésta propiedad asigna el atributo **id** del input, en caso no esté asignado tomará por defecto el valor de  name.
  
  - minLength
  > valor por defecto = 3
    Ésta propiedad asigna la cantidad mínima de caracteres con las que debe contar el input.
  
  - defaultValue
  > valor por defecto = ""  
    Ésta propiedad indica el valor por defecto que tendrá el input.
  
  - isDisabled
  > valor por defecto = false  
    Ésta propiedad indica si el input está deshabilitado o no.
  
  - hasNoLabel
  > valor por defecto = false  
    Ésta propiedad indica si el input no tiene **label**
  
  - placeholder
  > valor por defecto = ""  
    Ésta propiedad asigna un placeholder al input.
  
  - label
  > valor por defecto = ""  
    Ésta propiedad indica el valor del label para el input. 


### input_email  

  Éste mixin genera un input que solo permite el ingreso de correos electrónicos. Sus atributos son los siguientes.

  - name
  > valor por defecto = ""  
    Ésta propiedad es el nombre del input, debe ser el mismo que se ha declarado de lado de **backend**.

  - id
  > valor por defecto = name  
    Ésta propiedad asigna el atributo **id** del input, en caso no esté asignado tomará por defecto el valor de  name.
  
  - minLength
  > valor por defecto = 3
    Ésta propiedad asigna la cantidad mínima de caracteres con las que debe contar el input.
  
  - defaultValue
  > valor por defecto = ""  
    Ésta propiedad indica el valor por defecto que tendrá el input.
  
  - isDisabled
  > valor por defecto = false  
    Ésta propiedad indica si el input está deshabilitado o no.
  
  - hasNoLabel
  > valor por defecto = false  
    Ésta propiedad indica si el input no tiene **label**
  
  - placeholder
  > valor por defecto = ""  
    Ésta propiedad asigna un placeholder al input.
  
  - label
  > valor por defecto = ""  
    Ésta propiedad indica el valor del label para el input. 

### input_file  

  Éste mixin genera un input del tipo file, por ello es importante considerar que el **form** que lo contiene contenga el atributo **enctype="multipart/form-data"**  Sus atributos son los siguientes.

  - name
  > valor por defecto = ""  
    Ésta propiedad es el nombre del input, debe ser el mismo que se ha declarado de lado de **backend**.

  - id
  > valor por defecto = name  
    Ésta propiedad asigna el atributo **id** del input, en caso no esté asignado tomará por defecto el valor de  name.
  
  - minLength
  > valor por defecto = 3
    Ésta propiedad asigna la cantidad mínima de caracteres con las que debe contar el input.
  
  - defaultValue
  > valor por defecto = ""  
    Ésta propiedad indica el valor por defecto que tendrá el input.
  
  - isDisabled
  > valor por defecto = false  
    Ésta propiedad indica si el input está deshabilitado o no.
  
  - hasNoLabel
  > valor por defecto = false  
    Ésta propiedad indica si el input no tiene **label**
  
  - placeholder
  > valor por defecto = ""  
    Ésta propiedad asigna un placeholder al input.
  
  - label
  > valor por defecto = ""  
    Ésta propiedad indica el valor del label para el input.  

### list_checkbox  

  Éste mixin genera una lista de inputs del tipo checkbox. Sus atributos son los siguientes.

  - name
  > valor por defecto = ""  
    Ésta propiedad es el nombre del input, debe ser el mismo que se ha declarado de lado de **backend**.

  - id
  > valor por defecto = name  
    Ésta propiedad asigna el atributo **id** del input, en caso no esté asignado tomará por defecto el valor de  name.
  
  - minLength
  > valor por defecto = 3
    Ésta propiedad asigna la cantidad mínima de caracteres con las que debe contar el input.
  
  - hasNoLabel
  > valor por defecto = false  
    Ésta propiedad indica si el input no tiene **label**
  
  - label
  > valor por defecto = ""  
    Ésta propiedad indica el valor del label para el input. 


### checkbox  

  Éste mixin genera un input del tipo checkbox, a diferencia de **list_checkbox**, éste mixin se utiliza comunmente para el checkbox de términos y condiciones. Sus atributos son los siguientes.  

  - name
  > valor por defecto = ""  
    Ésta propiedad es el nombre del input, debe ser el mismo que se ha declarado de lado de **backend**.

  - id
  > valor por defecto = name  
    Ésta propiedad asigna el atributo **id** del input, en caso no esté asignado tomará por defecto el valor de  name.
  
  - minLength
  > valor por defecto = 3
    Ésta propiedad asigna la cantidad mínima de caracteres con las que debe contar el input.
  
  - hasNoLabel
  > valor por defecto = false  
    Ésta propiedad indica si el input no tiene **label**
  
  - label
  > valor por defecto = ""  
    Ésta propiedad indica el valor del label para el input. 

  - isRequired  
  > valor por defecto = false  
    Ésta propiedad indica si el checkbox es requerido o no.

  - has_link  
  > valor por defecto = false  
    Indica si el label de nuestro checkbox tendrá algún enlace o no.

  - link_href  
  > valor por defecto = ""  
    Aquí se indica la url del link a donde queremos apuntar. Por lo general es la url de términos y condiciones.

  - link_text  
  > valor por defecto = ""  
    Aquí va el texto que estará enlazando a la url declarada en el atributo anterior

### list_radiobox  
  
  Éste mixin genera una lista de inputs del tipo checkbox. Sus atributos son los siguientes.  

  - name
  > valor por defecto = ""  
    Ésta propiedad es el nombre del input, debe ser el mismo que se ha declarado de lado de **backend**.

  - id
  > valor por defecto = name  
    Ésta propiedad asigna el atributo **id** del input, en caso no esté asignado tomará por defecto el valor de  name.  
  
  - minLength
  > valor por defecto = 3
    Ésta propiedad asigna la cantidad mínima de caracteres con las que debe contar el input.
  
  - hasNoLabel
  > valor por defecto = false  
    Ésta propiedad indica si el input no tiene **label**
  
  - label
  > valor por defecto = ""  
    Ésta propiedad indica el valor del label para el input. 

### radiobox   
  
  Éste mixin genera un input del tipo checkbox. Sus atributos son los siguientes.  

  - name
  > valor por defecto = ""  
    Ésta propiedad es el nombre del input, debe ser el mismo que se ha declarado de lado de **backend**.

  - id
  > valor por defecto = name  
    Ésta propiedad asigna el atributo **id** del input, en caso no esté asignado tomará por defecto el valor de  name.  
  
  - minLength
  > valor por defecto = 3
    Ésta propiedad asigna la cantidad mínima de caracteres con las que debe contar el input.
  
  - hasNoLabel
  > valor por defecto = false  
    Ésta propiedad indica si el input no tiene **label**
  
  - label
  > valor por defecto = ""  
    Ésta propiedad indica el valor del label para el input.  

  - defaultValue
  > valor por defecto = ""  
    Ésta propiedad indica el valor por defecto que tendrá el input.  


### select   
  
  Éste mixin genera un input del tipo select. Sus atributos son los siguientes.  

  - name
  > valor por defecto = ""  
    Ésta propiedad es el nombre del input, debe ser el mismo que se ha declarado de lado de **backend**.

  - id
  > valor por defecto = name  
    Ésta propiedad asigna el atributo **id** del input, en caso no esté asignado tomará por defecto el valor de  name.  
  
  - minLength
  > valor por defecto = 3
    Ésta propiedad asigna la cantidad mínima de caracteres con las que debe contar el input.
  
  - valueSelected
  > valor por defecto = false  
    Ésta propiedad asigna un valor por defecto.

  - isAjax
  > valor por defecto = false  
    Ésta propiedad nos indica si el input efectúa una llamada del tipo ajax, usualmente utilizado para ubigeo.

  - dataSource
  > valor por defecto = ''  
    Ésta propiedad indica una URL a la que se hará una llamada AJAX si es que el atributo anterior fue asignado como TRUE.

  - isDisabled
  > valor por defecto = false  
    Ésta propiedad indica si el input está deshabilitado o no.

  - hasNoLabel
  > valor por defecto = false  
    Ésta propiedad indica si el input no tiene **label**
  
  - label
  > valor por defecto = ""  
    Ésta propiedad indica el valor del label para el input.  

  - defaultText
  > valor por defecto = ""  
    Ésta propiedad asigna un texto por defecto. Ésta propiedad es usualmente utilizada cuando el input select ha sido personalizado.

