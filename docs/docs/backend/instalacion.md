
# Instalación

	## Paso 1	
		Crear virtualenv 
			virtualenv --no-site-packages <project-dir-name>

	## Paso2
		Ir al directorio creado(<project-dir-name>) y clonar proyecto skelsus
		*	git clone git@bitbucket.org:no2developer/skelsus-django-site.git src
		**	git clone https://<username>@bitbucket.org/no2developer/skelsus-django-site.git src

	Al terminar el paso2 se tendrá la siguiente estructura

		project-dir-name
			bin/
			include/
		 	lib/
		 	local/
		 	src/
		  		backend/
					project/
						settings/
				  		base.py
						local.txt
						urls.py
						wsgy.py
					app/
					app_social/
					app_user/
					app_ubigeo/
					public/site/
					templates/
					requirements/
				  		local.txt
				  		production.txt
		  		frontend/

	## Paso3
		Virtualizar el proyecto
			source bin/activate

	## Paso4
		Ir al directorio src/backend/

	## Paso5
		Instalar requerimientos que el aplicativo usará.	
    		pip install -r requirements/(local|production).txt

	## Paso6
		Creat base de datos
			mysql -u<username> -p<password> 
    			CREATE DATABASE project_dir_name_db DEFAULT CHARACTER SET utf8

	## Paso7
		Renombrar el archivo project/settings/local.txt a 
		project/settings/local.py y cambiar las variables para el ambiente local.
			
		* accesos a la base de datos

			<database> = Nombre de la base de datos
			<username> = Nombre del usuario de base de datos
			<password> = Password del usuario de base de datos

			DATABASES = {
				'default': {
					'ENGINE': 'django.db.backends.mysql',
					'NAME': '<database>',
					'USER': '<username>',
					'PASSWORD': '<password>',
					'OPTIONS': {
						'init_command': 'SET storage_engine=INNODB',
					},
				}
			}

	## Paso8
		Ejecutar las migraciones de la base de datos
		python manage.py migrate --settings=project.settings.local	

	## Paso9
		Ejecutar el script para la carga de ubigeo(departamentos, provincias y distritos)
		python manage.py loadubigeo --settings=project.settings.local
