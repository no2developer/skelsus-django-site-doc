
# Módulos

	## Module app

		Es el módulo inicial que contiene toda la configuración del website
		Estructura
			-app/
				admin.py
			 	models.py
			 	tasks.py
			 	tests.py
			 	urls.py
			 	views.py
			 	template_context.py

		models.py
			Este archivo se encarga de generar los modelos iniciales del website tales como
				Site: title, ua analitycs, etc.
				Section: Secciones del App como news, catalog, profile user, register user, etc.

		views.py
			Este archivo es el controlador, prácticamente aquí solo se renderiza plantillas generales como 
				Home
				Thanks
				Winner, etc.

		urls.py 
			Se mostran las rutas que el website tendrá como 
				/	#home
				/gracias
				/terminos-y-condiciones etc.

		template_context.py
			Se configuran variables de contexto global que serán pasadas al template.

		tests.py
			Sirve para hacer pruebas unitarias.

		admin.py
			Se configura clases para mostrar el administrador del website.

	## Module app_user

		Este módulo se encarga de manejar toda la lógica referente al usuario
		Estructura		
			app_user/
		 		migrations/
		 		admin.py
		 		forms.py
		 		models.py
		 		tasks.py
		 		tests.py
		 		urls.py
		 		views.py

		admin.py 
			Se configurará clases para mostrar el administrador del website.
		
		forms.py 
			Se configurará formularios para ser trabajados por las vistas.

		models.py 
			Están los modelos que ña lógica de usuario usará
				User
					Información básica que un usuario tiene, depende del proyecto 
					se puede agregar o quitar propriedades.

		views.py
			Estan las clases que controlarán este aplicativo como 
				Login (Iniciar sessión con un usuario, entrar email y password)
				Logout (Salir de la sessión que el usuario se encuentra)
				Add(Agregar un nuevo usuario al site)
				Profile(Perfil del usuario)
				ForgotPassword(Controllador para manejar el tema de su olvidar clave)
				ResetPassword(Resetear el password del usuario, se envia un correo 
			 	para su nuevo password)
				Activate(Después de hacer click a la activación por correo, este controllador 
			 	se encarga de activar al usuario y permita loguearse)

		tests.py
			Sirve para hacer pruebas unitarias.
		
		urls.py
			Se mostran las rutas del módulo
				/iniciar-sesion
				/salir
				/perfil
				/activar-cuenta
				/crear-cuenta
				/olvidar-clave
				/resetear-clave

		migrations /
			Contiene todos los cambios de base de datos para ser ejecutados.


	## Module app_social

		Este módulo se encarga de la configuración de las redes sociales
		Estructura
		app_social/
		 	migrations/
			admin.py
		 	facebook.py
		 	twitter.py
		 	models.py
		 	urls.py
		 	views.py

		admin.py 
			Se configurará clases para mostrar el administrador del website.

		models.py
			Están los modelos que el módulo usará
				Social
					Configuración de las propiedades del aplicativo social.
				OpenGraph
					Contenido para shares, tweets realcionados a una sección
				UserFacebook
					Información Basica para grabar información de un usuario 
					Facebook relacionado a un usuario
				UserTwitter
					Información Basica para grabar información de un usuario 
					twitter relacionado a un usuario.
		urls.py
			Se mostrará las rutas que el aplicativo tiene
				/conectar-facebook
				/conectar-twitter

		views.py
			Se tiene clases como 
				FacebookConnect Conexion via backend
				FacebookJsConnect Conexion via Js
				TwitterConnect Conecion via backend

		facebook.py/twitter.py
			Tienen funciones basicas de api que ayudan a tener información de estas redes sociales
		
		migrations/
			 Contiene todos los cambios de base de datos para ser ejecutados-

	## Module app_ubigeo

		app_ubigeo/
			managment/
		   		commands/
		    		loadubigeo.py
		 	migrations/
		 	admin.py
		 	ubigeo.txt
		 	models.py
		 	urls.py
		 	views.py

		admin.py 
			Se configurará clases para mostrar el administrador del módulo.
		
		models.py
			Están los modelos que el app usará
				Ubigeo
					Contenido de departamentos, provincias y distritos
		ubigeo.txt
			se tiene toda los datos para ser cargados en la base de datos

		migrations
			contiene todos los cambios de base de datos para ser ejecutados. 

	## Project Folder
		
		project/
			settings/
		  	base.py
		  	local.py	#overrite
			urls.py
			wsgy.py	

		urls.py
			contiene las rutas agrupadas por todos los modulos del website:
				app.urls
				app_user.urls
				app_social.urls
				app_ubigeo.urls
				captcha
				admin, etc
		base.py
			tiene la configuración de variables de todo el proyecto.
		local.py
			tiene la configuración deñ ambiente local

	## Public Folder
		
		Contiene todos loa assets para el proyecto, estos tienen
			fuentes
			images
			scripts
			sprites
			styles
		y se mostrarán como ruta 
			http://domain.com/static/images
			http://domain.com/static/styles, etc

	## Requirements Folder
				
		requirements/
			local.txt
		 	production.txt

		En el archivo production.py se tiene un listado de las bibliotecas que el site requiere para que funcione en el servidor de producción
		En el archivo local.py se tiene un listado de las bibliotecas que el site requiere para que funcione en el servidor local

	## Templates Folder
		
		-templates/
		 -site/
		  -mailing/
		  -user/
		   -login.html
	           -etc....
		 -admin/
		 home.html
		 tyc.html

		-En la carpeta /site/user/ se tiene todos las plantillas(html) que el usuario requiere para ser mostrados.
		-En la carpeta /site/mailing/ se tiene todos las plantillas(html) para los envios de mailing.
		-En la carpeta /admin/ para sobreescribir cualquier template del django-admin.
		-En el archivo "home.html"  es el template principal de la aplicación(El home)
		-En el archivo "tyc.html" es la plantilla(html) de Terminos y Condiciones.

	## Logs.txt
		
		Aquí se graban todos los errores, advertencias que el aplicativo pueda tener en su funcionamiento.

	## manage.py
		Es el comando de django para realizar operacion con el framework.

